/*
 * Copyright (c) 2013 Christian Ferraz Lemos de Sousa
 *                    Iruan C. O. Silva
 *                    Lucas Gouvea
 *                    Marco
 *                    Vitor Moura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include "kriptonita.h"


int main(int argc, char **argv){
	char *key;
	const char *filename;

	switch(actions_menu())
	{
		case 1:
			key          = receive_key();
			filename     = get_filename();

			if (toggle_kripto(key, filename, false) == false) {
				printf("Arquivo salvo como 'cp_%s'.\n", filename);
			} else {
				printf("Ocorreu um erro.\n");
			}

			break;
		case 2:
			key          = receive_key();
			filename     = get_filename();

			if (toggle_kripto(key, filename, true) == 0) {
				printf("Arquivo salvo com 'dc_' antes de seu nome original.\n", filename);
			} else {
				printf("Ocorreu um erro.\n");
			}

			break;
		case 0:
			printf("Kriptonita v1.0.0.\n");
			break;
		default:
			#if OSNAME==0
				printf("Opção inexistente.\n");
			#else
				printf("Opcao inexistente.\n");
			#endif
			break;
	}
	return 0;
}
