/*
 * Copyright (c) 2013 Christian Ferraz Lemos de Sousa
 *                    Iruan C. O. Silva
 *                    Lucas Gouvea
 *                    Marco
 *                    Vitor Moura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "kriptonita.h"
#include "md5.h"

int actions_menu(){
	// Mensagens
	#if OSNAME==0
		printf("Escolha a ação a realizar:\n");
		printf("  (1) Criptografar\n");
		printf("  (2) Descriptografar\n");
		printf("  (0) Informações\n");
	#else
		printf("Escolha a acao a realizar:\n");
		printf("  (1) Criptografar\n");
		printf("  (2) Descriptografar\n");
		printf("  (0) Informacoes\n");
	#endif

	// Verificar Ação
	int actions;

	scanf("%d", &actions); // -1: erro

	if (actions < 0 && actions > 2) {
		actions = -1;
	}

	return (actions);
}

// erro, não sei porque esse código sumiu
void error(int code, char *message) {
	printf(message);
	printf("\n");
	exit(code);
}

// Receber chave
char *receive_key()
{
	#if OSNAME==0
		printf("Digite a chave de segurança:\n");
	#else
		printf("Digite a chave de seguranca:\n");
	#endif

	char *key = malloc(32768 * sizeof(char));

	char passw[8];
	scanf("%s", &passw);

	int index = 0;
	for (int i=0; i < 256; i++)
	{
		unsigned int *subkey = md5(&passw[i%8], 0);
		for (int ii=0; ii < 128; ii++)
		{
			// criar chave gigante para dificultar identificação de repetições
			key[index] = ((((((subkey[ii]%255)^SALT)*i)+SALT)^i)*SALT)^SALT;
			index++;
		}
	}

	return key;
}


// função geral de criptografia/descriptografia
int toggle_kripto(char *key, const char *filename, bool decript){
	char *out_filename = malloc(256 * sizeof(char));

	for(int i=0; i < 256; i++){
		switch(i){
			case 0:
				out_filename[i] = *"c";
				if (decript==true){
					out_filename[i] = *"d";
				}
				break;
			case 1:
				out_filename[i] = *"p";
				if (decript==true){
					out_filename[i] = *"c";
				}
				break;
			case 2:
				out_filename[i] = *"_";
				break;
			default:
				if (decript == true) {
					out_filename[i] = filename[i];
				} else {
					out_filename[i] = filename[i-3];
				}
		}
	}

	FILE *fin = fopen(filename, "r");
	FILE *fout = fopen(out_filename, "w+");

	if (fin==NULL) error (-1, "Erro ao tentar abrir o arquivo");
	if (fout==NULL) error (-1, "Erro ao tentar abrir o arquivo");

	char chin;
	char chout;

	off_t filesize = fsize(filename);

	printf("%d\n", filesize);

	for (off_t i = 0; i <= filesize; i++)
	{
	    chin = fgetc(fin);
		chout = chin^key[i%32768];
		fputc(chout, fout);
	}

	fclose(fin);
	fclose(fout);
	return 0;
}


// receber nome do arquivo
char *get_filename()
{
	printf("Digite o nome do arquivo:\n");
	char *fname = malloc(253 * sizeof(char));
	scanf("%s", fname);
	return fname;
}

// checar tamanho do arquivo
off_t fsize(const char *filename) {
    struct stat st;

    if (stat(filename, &st) == 0)
        return st.st_size-1;

	#if OSNAME==0
    	fprintf(stderr, "Não pode determinar o tamanho de %s: %s\n",
        	    filename, strerror(errno));
    #else
    	fprintf(stderr, "Nao pode determinar o tamanho de %s: %s\n",
        	    filename, strerror(errno));
    #endif

    return -1;
}