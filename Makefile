#
# Copyright (c) 2013 Christian Ferraz Lemos de Sousa
#                    Iruan C. O. Silva
#                    Lucas Gouvea
#                    Marco
#                    Vitor Moura
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# Configurações do Compilador
SRC    = ./$(wildcard src/*.c)
OBJ    = $(SRC:.c=.o)
SALT   = 13814493723833933
CFLAGS = -Os -Wextra -std=c99 -D SALT=$(SALT)

# Portabilidade Operacional
CFLAGS += -Os
CLIBS   = -lm

ifeq ($(OS), Windows_NT)
	OSNAME  = 1
	FNAME   = kriptonita.exe
else
	OSNAME  = 0
	FNAME   = kriptonita
endif

#
# Regras de Construção
#

# Regra Padrão
default: all

# Construção de Objeto
./src/%.o:
	$(CC) $(CFLAGS) -c $(@:.o=.c) -DOSNAME=$(OSNAME) -o $@

# Link de Objetos
link: $(OBJ)
	mkdir -p ./bin/
	$(CC) $(CFLAGS) $(OBJ) $(CLIBS) -DOSNAME=$(OSNAME) -o ./bin/$(FNAME)

# Limpesa de Restos (mortais)
clean:
	-rm -r ./bin/ $(OBJ);

test:
	@./bin/$(FNAME)

# Realizar tudo do Começo
all: $(OBJ) link