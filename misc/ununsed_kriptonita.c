/*
 * Copyright (c) 2013 Christian Ferraz Lemos de Sousa
 *                    Iruan C. O. Silva
 *                    Lucas Gouvea
 *                    Marco
 *                    Vitor Moura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "kriptonita.h"
#include "md5.h"

int actions_menu(){
	// Mensagens
	printf("Escolha a ação a realizar:\n");
	printf("  (1) Criptografar\n");
	printf("  (2) Descriptografar\n");
	printf("  (0) Informações\n");

	// Verificar Ação
	int actions;

	scanf("%d", &actions); // -1: erro

	if (actions < 0 && actions > 2) {
		actions = -1;
	}

	return (actions);
}

char *receive_key()
{
	printf("Digite a chave de segurança:\n");

	char *key = malloc(32768);

	char passw[8];
	scanf("%s", &passw);

	int index = 0;
	for (int i=0; i < 256; i++)
	{
		unsigned *subkey = md5(&passw[i%8], 0);
		for (int ii=0; ii < 128; ii++)
		{
			// criar chave gigante para dificultar identificação de repetições
			key[index] = (((((subkey[ii]^SALT)*i)+SALT)^i)*SALT)^SALT;
			index++;
		}
	}

	return key;
}

char encript_file(char *key)
{
	const char *filename = get_filename();

	char out_filename[3+256] = "cp_";

	for (int i=0; i < 256; i++) {
		out_filename[i+3] = filename[i];
	}

	FILE *fin = fopen(filename, "r");
	FILE *fout = fopen(out_filename, "w+");
	//if (pFile==NULL) perror ("Error opening file");

	char chin;
	char chout;

	/*
	// Método antigo que usa função de
	// checar tamanho de arquivo
	for (off_t i = 0; i < fsize(filename); i++)
	{
	    chin = fgetc(fin);
		chout = chin^key[i%32768];
		fputc(chout, fout);
	}*/

	int i =0;
	while ((chin = fgetc(fin)) != EOF)
	{
		chout = chin^key[i%32768];
		fputc(chout, fout);
		i++;
	}

	fclose(fin);
	fclose(fout);
	return 0;
}

char decript_file(char *key)
{
	const char *filename = get_filename();

	char out_filename[3+256] = "dc_";

	for (int i=0; i < 256; i++) {
		out_filename[i+3] = filename[i+3];
	}

	/*for (off_t i=0; i < fsize(filename); i++) {
		out_filename[i+9] = filename[i];
	}*/

	FILE *fin = fopen(filename, "r");
	FILE *fout = fopen(out_filename, "w+");

	char chin;
	char chout;

	int i = 0;
	do
	{
		chin = fgetc(fin);
		chout = chin^key[i%32768];
		fputc(chout, fout);
		i++;
	} while (chout != EOF);

	fclose(fin);
	fclose(fout);
	return 0;
}

char save_data(char *data, char *fname)
{
	return 0;
}


char *get_filename()
{
	printf("Digite o nome do arquivo:\n");

	char *fname = malloc(256);

	scanf("%s", fname);

	return fname;
}

// checar tamanho do arquivo
off_t fsize(const char *filename) {
    struct stat st;

    if (stat(filename, &st) == 0)
        return st.st_size-1;

    fprintf(stderr, "Cannot determine size of %s: %s\n",
            filename, strerror(errno));

    return -1;
}